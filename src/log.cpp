#include <stream9/log.hpp>
#include <stream9/log/grabber.hpp>

#include <sstream>
#include <regex>

#include <boost/test/unit_test.hpp>

#include <stream9/strings/stream.hpp>
#include <stream9/strings/ends_with.hpp>

namespace testing {

namespace log = stream9::log;
namespace str = stream9::strings;

class ostream_captureer
{
public:
    // essentials
    ostream_captureer(std::ostream& os, std::string& s)
        : m_os { &os }
        , m_buf { s }
    {
        m_old_buf = os.rdbuf(&m_buf);
    }

    ~ostream_captureer() noexcept
    {
        stop();
    }

    // modifier
    void stop()
    {
        if (m_old_buf) {
            *m_os << std::flush;
            m_os->rdbuf(m_old_buf);
            m_old_buf = nullptr;
        }
    }

private:
    std::ostream* m_os;
    str::streambuf<std::string> m_buf;
    std::streambuf* m_old_buf = nullptr;
};

BOOST_AUTO_TEST_SUITE(log_)

    BOOST_AUTO_TEST_CASE(dbg_)
    {
        std::string result;
        {
            ostream_captureer s { std::cerr, result };

            log::info() << "hello" << "world";
        }

        BOOST_TEST(str::ends_with(result, "hello world\n"));
    }

    BOOST_AUTO_TEST_CASE(iomanip_)
    {
        log::info() << std::flush;
    }

    BOOST_AUTO_TEST_CASE(nospace_)
    {
        std::string result;
        {
            ostream_captureer s { std::cerr, result };

            log::info() << log::nospace << "a" << "b" << "c";
        }

        BOOST_TEST(str::ends_with(result, "abc\n"));
    }

    BOOST_AUTO_TEST_CASE(nocr_)
    {
        std::string result;
        {
            ostream_captureer s { std::cerr, result };

            log::info() << log::nocr << "a" << "b";
            log::info() << "c";
        }

        BOOST_TEST(result == "a bc\n");
    }

    bool alt_handler(log::priority const pri,
                     std::source_location const& loc,
                     std::string_view const,
                     std::string_view const message)
    {
        (void)pri;

        std::cout << loc.file_name() << ":"
                  << loc.line() << ":"
                  << message;
        return true;
    }

    BOOST_AUTO_TEST_CASE(alternative_handler_)
    {
        std::string result;
        {
            ostream_captureer s { std::cout, result };

            auto old_handler = log::set_handler(alt_handler);

            log::dbg() << "foo";

            log::set_handler(old_handler);
        }

        std::regex pattern {
            R"(\.\./src/log\.cpp:\d+:foo\n)"
        };
    }

BOOST_AUTO_TEST_SUITE_END() // log_

BOOST_AUTO_TEST_SUITE(grabber_)

    BOOST_AUTO_TEST_CASE(recover_handler_after_destruction_)
    {
        std::string result;
        {
            ostream_captureer s { std::cerr, result };

            log::info() << "before";
            {
                log::grabber g;
                log::info() << "foo";

                BOOST_TEST(g.info().size() == 1);
            }
            log::info() << "after";
        }

        BOOST_TEST(result == "before\nafter\n");
    }

    BOOST_AUTO_TEST_CASE(debug_)
    {
        log::grabber g;

        log::dbg() << "foo" << "bar";
        log::dbg() << "xyzzy";

        auto const expected = { "foo bar\n", "xyzzy\n" };
        BOOST_TEST(g.debug() == expected, boost::test_tools::per_element());
    }

    BOOST_AUTO_TEST_CASE(info_)
    {
        log::grabber g;

        log::info() << "foo" << "bar";
        log::info() << "xyzzy";

        auto const expected = { "foo bar\n", "xyzzy\n" };
        BOOST_TEST(g.info() == expected, boost::test_tools::per_element());
    }

    BOOST_AUTO_TEST_CASE(warning_)
    {
        log::grabber g;

        log::warn() << "foo" << "bar";
        log::warn() << "xyzzy";

        auto const expected = { "foo bar\n", "xyzzy\n" };
        BOOST_TEST(g.warning() == expected, boost::test_tools::per_element());
    }

    BOOST_AUTO_TEST_CASE(error_)
    {
        log::grabber g;

        log::err() << "foo" << "bar";
        log::err() << "xyzzy";

        auto const expected = { "foo bar\n", "xyzzy\n" };
        BOOST_TEST(g.error() == expected, boost::test_tools::per_element());
    }

BOOST_AUTO_TEST_SUITE_END() // grabber_

} // namespace testing
